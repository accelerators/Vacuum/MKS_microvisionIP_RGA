#
#   This file execute a git status on each sub class
#


CheckStatus() {
    echo "==================================================="
    echo "./$PROJECT:"
    cd external/$PROJECT
    git status
    echo ""
    cd ../..
}


#=====================================
#
#            Main part
#
#=====================================
echo "================ MKS_microvisionIP_RGA ==================="
git status
    
PROJECT=Socket
CheckStatus
