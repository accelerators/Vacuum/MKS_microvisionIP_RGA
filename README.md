# Project MKS_microvisionIP_RGA

Cpp project

This device server will control MKS Microvision IP RGA (Residual Gas Analyser).
The control is done though a Socket device.

## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/Vacuum/MKS_microvisionIP_RGA
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
* Socket TANGO class
  

#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build. 
  

### Build

Instructions on building the project.

#### Retrieving the Socket class

```bash
bash clone-classes.bash
```

#### Checking the GIT status of the different classes

```bash
bash global-status.bash
```

#### Building MKS_MicrovisionIP_RGA Device Server Executable

CMake example: 

```bash
cd project_name
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ../
make -j
```

Make example: 

```bash
cd project_name
make -j
```

