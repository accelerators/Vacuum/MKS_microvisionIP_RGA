# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [MKS_MicrovisionIP_RGA-2.1] - 16/01/2023
### Fixed
- Fix a bug leading to server not starting when the timeout property of MKS_MicrovisionIP_RGA devices was set to a value >= 2148ms.
An uncaught omni_thread_fatal exception was thrown by omni_condition::timedwait() in this use case.
In addition to the bug fix, omni_thread_fatal exceptions are now caught in check_ACK() method and the ERRNO set by the 
pthread_cond_timedwait() method called by omni_condition::timedwait is logged and returned in a DevFailed exception.

## [MKS_MicrovisionIP_RGA-2.0] - 15/05/2022
### Fixed
- No longer exits when there is a Socket initialization problem

### Added
- Added clone-classes.bash and global-status.bash to help retrieving the code and checking the git status of the Socket class dependency.

### Changed
- The Makefiles and xmi files have been modified to use the Socket class files coming from the external/Socket directory.
- The CMakeLists.txt file no longer imposes the -g compilation flag. The user can use -DCMAKE_BUILD_TYPE to build a 
Debug or Release or RelWithDebInfo release if wanted.

## [MKS_MicrovisionIP_RGA-1.3] - 20/08/2020
### Fixed
- Fix bug leading to strange characters displayed in the Status

## [MKS_MicrovisionIP_RGA-1.2] - 07/07/2020
### Fixed
- Fix bug in Status command/attribute which was leading to Status command/attribute being always "Device is OK"

### Added
- Added License file

## [MKS_MicrovisionIP_RGA-1.1] - 15/01/2020
### Changed
- Replace a Tango::Except::print_exception with an ERROR_STREAM message to avoid the Starter logs to be too big when trying to fill the polling attr buffer history for attributes which are not configured with polling externally triggered.

## [MKS_MicrovisionIP_RGA-1.0] - 20/11/18
- Initial Revision in GIT
